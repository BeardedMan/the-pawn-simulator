import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PawnSimulatorControlsComponent } from './pawn-simulator-controls.component';

describe('PawnSimulatorControlsComponent', () => {
  let component: PawnSimulatorControlsComponent;
  let fixture: ComponentFixture<PawnSimulatorControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PawnSimulatorControlsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PawnSimulatorControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onMove', () => {
    beforeEach(() => {
      spyOn(component.move$, 'next');
    });

    it('should emit event on move$ output', () => {
      component.onMove();

      expect(component.move$.next).toHaveBeenCalled();
    });

    it('should change the variable isFirstMove to false', () => {
      component.onMove();

      expect(component.isFirstMove).toBeFalse();
    });
  });
});
