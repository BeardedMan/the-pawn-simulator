import { ChangeDetectionStrategy, Component, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pawn-simulator-controls',
  templateUrl: './pawn-simulator-controls.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PawnSimulatorControlsComponent {
  isFirstMove = true;

  @Output() move$ = new Subject<number>();
  @Output() left$ = new Subject();
  @Output() right$ = new Subject();
  @Output() report$ = new Subject();

  onMove(units?: number): void {
    this.move$.next(units);
    this.isFirstMove = false;
  }
}
