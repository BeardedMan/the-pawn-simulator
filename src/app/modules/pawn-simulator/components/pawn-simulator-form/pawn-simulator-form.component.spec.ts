import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PawnSimulatorFormComponent } from './pawn-simulator-form.component';

describe('PawnSimulatorFormComponent', () => {
  let component: PawnSimulatorFormComponent;
  let fixture: ComponentFixture<PawnSimulatorFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PawnSimulatorFormComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PawnSimulatorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
