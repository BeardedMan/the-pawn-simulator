import { ChangeDetectionStrategy, Component, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { Facing, PawnColour } from 'src/app/modules/pawn-simulator/models/pawn.model';

@Component({
  selector: 'app-pawn-simulator-form',
  templateUrl: './pawn-simulator-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PawnSimulatorFormComponent {
  @Input() formGroup: FormGroup;

  @Output() submit$ = new Subject();

  readonly facingKeys = Object.keys(Facing);
  readonly pawnColourKeys = Object.keys(PawnColour);
}
