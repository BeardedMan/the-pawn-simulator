import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PawnSimulatorComponent } from './pawn-simulator.component';
import { FormBuilder, FormControl } from '@angular/forms';
import { PawnService } from 'src/app/modules/pawn-simulator/services/pawn.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Facing } from 'src/app/modules/pawn-simulator/models/pawn.model';
import { Router } from '@angular/router';

describe('PawnSimulatorComponent', () => {
  let component: PawnSimulatorComponent;
  let pawnService: PawnService;
  let fixture: ComponentFixture<PawnSimulatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PawnSimulatorComponent],
      providers: [FormBuilder, PawnService],
      imports: [RouterTestingModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PawnSimulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    pawnService = TestBed.inject(PawnService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should run method initializeForm and create pawnFormGroup', () => {
      fixture.detectChanges();

      expect(component.pawnFormGroup).not.toBeUndefined();
    });
  });

  describe('onSubmit', () => {
    it('should change the value of isFormSubmitted variable to true', () => {
      component.onSubmit();

      expect(component.isFormSubmitted).toBeTrue();
    });
  });

  describe('onMove', () => {
    const mockPosition = { positionX: 2, positionY: 3 };

    beforeEach(() => {
      spyOn(pawnService, 'movePawnHandler').and.returnValue(mockPosition);
      spyOn(component.pawnFormGroup, 'get').and.returnValue(new FormControl(''));
      spyOn(component.pawnFormGroup, 'patchValue');
    });

    it('should run get methods on pawnFormGroup variable', () => {
      component.onMove();

      expect(component.pawnFormGroup.get).toHaveBeenCalledWith('facing');
      expect(component.pawnFormGroup.get).toHaveBeenCalledWith('positionX');
      expect(component.pawnFormGroup.get).toHaveBeenCalledWith('positionY');
    });

    it('should run PawnService method movePawnHandler', () => {
      component.onMove();

      expect(pawnService.movePawnHandler).toHaveBeenCalled();
    });

    it('should run method patchValue to update the form group value', () => {
      component.onMove();

      expect(component.pawnFormGroup.patchValue).toHaveBeenCalledWith(mockPosition);
    });
  });

  describe('onLeft', () => {
    const mockFacing = Facing.WEST;
    const mockControl = new FormControl();

    beforeEach(() => {
      spyOn(pawnService, 'leftRotateHandler').and.returnValue(mockFacing);
      spyOn(component.pawnFormGroup, 'get').and.returnValue(mockControl);
      spyOn(mockControl, 'setValue');
    });

    it('should run get methods on pawnFormGroup variable', () => {
      component.onLeft();

      expect(component.pawnFormGroup.get).toHaveBeenCalledWith('facing');
    });

    it('should run PawnService method leftRotateHandler', () => {
      component.onLeft();

      expect(pawnService.leftRotateHandler).toHaveBeenCalled();
    });

    it('should run method setValue to update the form control value', () => {
      component.onLeft();

      expect(mockControl.setValue).toHaveBeenCalledWith(mockFacing);
    });
  });

  describe('onRight', () => {
    const mockFacing = Facing.EAST;
    const mockControl = new FormControl();

    beforeEach(() => {
      spyOn(pawnService, 'rightRotateHandler').and.returnValue(mockFacing);
      spyOn(component.pawnFormGroup, 'get').and.returnValue(mockControl);
      spyOn(mockControl, 'setValue');
    });

    it('should run get methods on pawnFormGroup variable', () => {
      component.onRight();

      expect(component.pawnFormGroup.get).toHaveBeenCalledWith('facing');
    });

    it('should run PawnService method rightRotateHandler', () => {
      component.onRight();

      expect(pawnService.rightRotateHandler).toHaveBeenCalled();
    });

    it('should run method setValue to update the form control value', () => {
      component.onRight();

      expect(mockControl.setValue).toHaveBeenCalledWith(mockFacing);
    });
  });

  describe('onReport', () => {
    let router: Router;

    beforeEach(() => {
      router = TestBed.inject(Router);
      spyOn(router, 'navigate').and.callFake(() => Promise.resolve(true));
    });

    it('should navigate to home path', () => {
      component.onReport();

      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
  });
});
