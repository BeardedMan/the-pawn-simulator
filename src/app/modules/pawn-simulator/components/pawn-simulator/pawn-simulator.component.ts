import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Facing, PawnColour } from 'src/app/modules/pawn-simulator/models/pawn.model';
import { PawnService } from 'src/app/modules/pawn-simulator/services/pawn.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pawn-simulator',
  templateUrl: './pawn-simulator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PawnSimulatorComponent implements OnInit {
  isFormSubmitted = false;
  pawnFormGroup: FormGroup;

  constructor(private fb: FormBuilder, private pawnService: PawnService, private router: Router) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
  }

  onMove(units?: number): void {
    const facing = this.pawnFormGroup.get('facing');
    const positionX = this.pawnFormGroup.get('positionX');
    const positionY = this.pawnFormGroup.get('positionY');
    const position = this.pawnService.movePawnHandler(facing?.value, positionY?.value, positionX?.value, units);

    this.pawnFormGroup.patchValue({ ...position });
  }

  onLeft(): void {
    const facing = this.pawnFormGroup.get('facing');
    const facingAfterRotation = this.pawnService.leftRotateHandler(facing?.value);

    facing?.setValue(facingAfterRotation);
  }

  onRight(): void {
    const facing = this.pawnFormGroup.get('facing');
    const facingAfterRotation = this.pawnService.rightRotateHandler(facing?.value);

    facing?.setValue(facingAfterRotation);
  }

  onReport(): void {
    // tslint:disable-next-line:no-console
    console.clear();
    // tslint:disable-next-line:no-console
    console.info('Final place of the pawn: ', this.pawnFormGroup?.getRawValue());

    this.router.navigate(['/']);
  }

  private initializeForm(): void {
    this.pawnFormGroup = this.fb.group({
      positionX: [0, [Validators.required, Validators.max(8), Validators.min(0)]],
      positionY: [0, [Validators.required, Validators.max(8), Validators.min(0)]],
      facing: [Facing.NORTH, Validators.required],
      colour: [PawnColour.WHITE, Validators.required]
    });
  }
}
