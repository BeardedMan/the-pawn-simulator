export enum Facing {
  NORTH = 'NORTH',
  SOUTH = 'SOUTH',
  WEST = 'WEST',
  EAST = 'EAST'
}

export enum PawnColour {
  WHITE = 'WHITE',
  BLACK = 'BLACK'
}
