import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PawnSimulatorComponent } from 'src/app/modules/pawn-simulator/components/pawn-simulator/pawn-simulator.component';

const routes: Routes = [{ path: '', component: PawnSimulatorComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PawnSimulatorRoutingModule {}
