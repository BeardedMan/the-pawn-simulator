import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PawnSimulatorComponent } from 'src/app/modules/pawn-simulator/components/pawn-simulator/pawn-simulator.component';
import { PawnSimulatorRoutingModule } from 'src/app/modules/pawn-simulator/pawn-simulator-routing.module';
import { PawnSimulatorControlsComponent } from './components/pawn-simulator-controls/pawn-simulator-controls.component';
import { PawnSimulatorFormComponent } from './components/pawn-simulator-form/pawn-simulator-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PawnService } from 'src/app/modules/pawn-simulator/services/pawn.service';

@NgModule({
  declarations: [PawnSimulatorComponent, PawnSimulatorControlsComponent, PawnSimulatorFormComponent],
  imports: [CommonModule, PawnSimulatorRoutingModule, ReactiveFormsModule],
  providers: [PawnService]
})
export class PawnSimulatorModule {}
