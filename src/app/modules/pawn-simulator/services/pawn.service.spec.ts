import { TestBed } from '@angular/core/testing';
import { PawnService } from './pawn.service';
import { Facing } from 'src/app/modules/pawn-simulator/models/pawn.model';

describe('PawnService', () => {
  let service: PawnService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PawnService]
    });
    service = TestBed.inject(PawnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('leftRotateHandler', () => {
    it('should rotate the NORTH facing to 90 degrees to WEST', () => {
      expect(service.leftRotateHandler(Facing.NORTH)).toEqual(Facing.WEST);
    });

    it('should rotate the SOUTH facing to 90 degrees to EAST', () => {
      expect(service.leftRotateHandler(Facing.SOUTH)).toEqual(Facing.EAST);
    });

    it('should rotate the WEST facing to 90 degrees to SOUTH', () => {
      expect(service.leftRotateHandler(Facing.WEST)).toEqual(Facing.SOUTH);
    });

    it('should rotate the EAST facing to 90 degrees to NORTH', () => {
      expect(service.leftRotateHandler(Facing.EAST)).toEqual(Facing.NORTH);
    });
  });

  describe('rightRotateHandler', () => {
    it('should rotate the NORTH facing to 90 degrees to EAST', () => {
      expect(service.rightRotateHandler(Facing.NORTH)).toEqual(Facing.EAST);
    });

    it('should rotate the SOUTH facing to 90 degrees to WEST', () => {
      expect(service.rightRotateHandler(Facing.SOUTH)).toEqual(Facing.WEST);
    });

    it('should rotate the WEST facing to 90 degrees to NORTH', () => {
      expect(service.rightRotateHandler(Facing.WEST)).toEqual(Facing.NORTH);
    });

    it('should rotate the EAST facing to 90 degrees to SOUTH', () => {
      expect(service.rightRotateHandler(Facing.EAST)).toEqual(Facing.SOUTH);
    });
  });

  describe('movePawnHandler', () => {
    const units = 1;
    let facing = Facing.NORTH;
    let mockPositionX = 0;
    let mockPositionY = 0;

    beforeEach(() => {
      facing = Facing.NORTH;
      mockPositionX = 0;
      mockPositionY = 0;
    });

    it('should add units to Y position in case of NORTH facing', () => {
      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY + units
      });
    });

    it('should not add units to Y position in case of NORTH facing and chess board edge', () => {
      mockPositionY = 8;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY
      });
    });

    it('should minus units from Y position in case of SOUTH facing', () => {
      mockPositionY = 8;
      facing = Facing.SOUTH;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY - units
      });
    });

    it('should not minus units from Y position in case of SOUTH facing and chess board edge', () => {
      facing = Facing.SOUTH;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY
      });
    });

    it('should minus units from X position in case of WEST facing', () => {
      mockPositionX = 8;
      facing = Facing.WEST;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX - units,
        positionY: mockPositionY
      });
    });

    it('should not minus units from X position in case of WEST facing and chess board edge', () => {
      facing = Facing.WEST;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY
      });
    });

    it('should add units to X position in case of EAST facing', () => {
      facing = Facing.EAST;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX + units,
        positionY: mockPositionY
      });
    });

    it('should not add units to X position in case of EAST facing and chess board edge', () => {
      mockPositionX = 8;
      facing = Facing.EAST;

      expect(service.movePawnHandler(facing, mockPositionY, mockPositionX, units)).toEqual({
        positionX: mockPositionX,
        positionY: mockPositionY
      });
    });
  });
});
