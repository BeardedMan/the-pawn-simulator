import { Injectable } from '@angular/core';
import { Facing } from 'src/app/modules/pawn-simulator/models/pawn.model';

@Injectable()
export class PawnService {
  leftRotateHandler(facing: Facing): Facing {
    switch (facing) {
      case Facing.NORTH:
        return Facing.WEST;
      case Facing.SOUTH:
        return Facing.EAST;
      case Facing.WEST:
        return Facing.SOUTH;
      case Facing.EAST:
        return Facing.NORTH;
    }
  }

  rightRotateHandler(facing: Facing): Facing {
    switch (facing) {
      case Facing.NORTH:
        return Facing.EAST;
      case Facing.SOUTH:
        return Facing.WEST;
      case Facing.WEST:
        return Facing.NORTH;
      case Facing.EAST:
        return Facing.SOUTH;
    }
  }

  movePawnHandler(
    facing: Facing,
    positionY: number,
    positionX: number,
    units = 1
  ): { positionY: number; positionX: number } {
    switch (facing) {
      case Facing.NORTH:
        if (positionY <= 8 - units) {
          positionY = positionY + units;
        }
        return { positionY, positionX };
      case Facing.SOUTH:
        if (positionY >= units) {
          positionY = positionY - units;
        }
        return { positionY, positionX };
      case Facing.WEST:
        if (positionX >= units) {
          positionX = positionX - units;
        }
        return { positionY, positionX };
      case Facing.EAST:
        if (positionX <= 8 - units) {
          positionX = positionX + units;
        }
        return { positionY, positionX };
    }
  }
}
